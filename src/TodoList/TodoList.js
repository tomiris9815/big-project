import React from "react";
import "./TodoList.scss";
export default function TodoList({ list, deleteTodo, done,edit}) {
  // ( props )
  return (
    <table className="TodoList w3-table w3-striped w3-bordered">     
        <tbody >
            <tr className="TodoList__title">
                <td>ID</td>
                <td>Name</td>
                <td>Actions</td>
            </tr>

            {list.map((todo, i) => (
            <tr
                key={i}
                className={`TodoList__elem ${todo.done ? "TodoList__elem--done" : ""} `}
            >
                <td>{i+1}</td>
                <td> {todo.name}</td>
                <td className="TodoList__actions">
                <div className="TodoList__table">
                    <input type="checkbox"className="App__toggle__button" onClick={() => done(i)}/>
                    <button  className="App__edit__button" onClick={()=>edit(i)} >
                        <img className="editImage" src={require('./edit.png') }/>
                    </button>
                    <button className="App__delete__button"onClick={() => deleteTodo(i)}>
                        <img className="editImage" src={require('./delete.png') }/>
                    </button>
                </div>
                </td>
            </tr>
            ))}
        </tbody>
    </table>
  );
}
